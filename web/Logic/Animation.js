var Animation = function(duration, imageSequence, creator, canvasCtx){
    this.x = creator.x;
    this.y = creator.y;
    this.animationEnded = false;
    
    var state = 0;
    var stateCounter = 0;
    var transitionInterval;
    
    transitionInterval = duration*60/imageSequence.length;
    
    this.transition = function(){
        stateCounter++;
        state = Math.round(stateCounter/transitionInterval);
        
        if(state === imageSequence.length) this.animationEnded = true;
    };
    
    this.render = function(){
        if(!this.animationEnded) canvasCtx.drawImage(imageSequence[state], this.x, this.y);
    };
};