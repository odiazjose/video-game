/* global enemyCanvas, enemyArray, enemyAttacks, sfxArray, hitSequence5 */

var Enemy = function(y){
    var classType = 0;
    
    this.x = enemyCanvas.width;
    this.y = y || Math.random()*enemyCanvas.height;
    
    this.health;
    var speedX;
    var speedY;
    
    var damage;
    var fireRate;
    var fireReady;
    var fireCooldown;
    
    var atkSprite = new Image();
    var atkSpriteURI;
    this.hitSFX;
    
    var sprite = new Image();
    var spriteURI;
    this.width;
    this.height;
    
    var canvasContext = enemyCanvas.getContext("2d");
    
    switch(classType){
        case 0:
            this.health = 5;
            speedX = 5;
            speedY = 4;
            
            damage = 2;
            fireRate = 0.6;
            
            this.width = 150;
            this.height = 75;
            
            this.hitSFX = hitSequence5;
            spriteURI = "Resources/Enemy1.png";
            atkSprite.src = "Resources/Laser5.png";
            break;
    }
    
    sprite.src = spriteURI;
    
    fireReady = 60/fireRate;
    fireCooldown = fireReady;
    
    this.render = function(){
        canvasContext.drawImage(sprite, this.x, this.y, this.width, this.height);
    };
    
    this.move = function(){
        this.x -= speedX;
    };
    
    this.shoot = function(){
        fireCooldown += 1;
        
        if(fireCooldown >= fireReady){
            enemyAttacks.push(new Attack(this.x - 35, this.y + 37, -8, damage, 30, 15, enemyAttacks, atkSprite));
            fireCooldown = 0;
        }
    };
    
    this.damage = function(damage){
        this.health -= damage;
    };
};