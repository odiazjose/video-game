var GameCanvas = function(width, height, container, ID){
    var canvas = document.createElement("canvas");
    
    canvas.width = width;
    canvas.height = height;
    
    container.appendChild(canvas);
    
    canvas.id = ID || "gameCanvas";
    
    return canvas;
};