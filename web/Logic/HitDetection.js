function forwardShotCollision(shot, collidedObj){
    if(shot.x + shot.width > collidedObj.x)
        if((shot.y + shot.height > collidedObj.y) && (shot.y < collidedObj.y + collidedObj.height))
            if(shot.x < collidedObj.x + collidedObj.width){
                collidedObj.damage(shot.damage);
                shot.hit = true;
            }
};

function backwardShotCollision(shot, collidedObj){
    if(shot.x < collidedObj.x + collidedObj.width)
        if((shot.y + shot.height > collidedObj.y) && (shot.y < collidedObj.y + collidedObj.height))
            if(shot.x > collidedObj.x){
                collidedObj.damage(shot.damage);
                shot.hit = true;
            }
};

function collision(obj1, collidedObj, damage){
    if(obj1.x + obj1.width > collidedObj.x)
        if((obj1.y + obj1.height > collidedObj.y) && (obj1.y < collidedObj.y + collidedObj.height))
            if(obj1.x < collidedObj.x + collidedObj.width)
                collidedObj.damage(damage);
};