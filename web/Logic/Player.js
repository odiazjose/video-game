/* global playerCanvas, playerArray, hitSequence1, hitSequence2, hitSequence3, hitSequence4, UICanvas */

var Player = function(playerNumber, playerSessionID, initialX, initialY, classType){
    var playerCanvasContext = playerCanvas.getContext("2d");
    var UIContext = UICanvas.getContext("2d");
    var UIseparation;
    
    this.playerSessionID = "" + playerSessionID;
    this.playerNumber = playerNumber;
    this.score = 0;
    
    this.x = initialX;
    this.y = initialY;
    
    var spaceShip = new Image();
    var spaceShipURI;
    this.width;
    this.height;
    
    var atkSprite = new Image();
    var atkSpriteURI;
    this.hitSFX;
    
    var controls = {left: false, right: false, up: false, down: false, shoot: false, shift: false};
    
    var speed;
    var fireRate;
    var fireReady;
    var fireCooldown;
    var damage;
    this.health;
    
    this.attacks = [];
    var attack;
    
    this.shoot;
    
    switch(classType){
        case 1:
            this.width = 92;
            this.height = 86;
            
            spaceShipURI = "Resources/Class1.png";
            atkSpriteURI = "Resources/Laser1.png";
            this.hitSFX = hitSequence1;
            
            speed = 13;
            fireRate = 1.7;
            damage = 2;
            this.health = 5;
            
            this.shoot = function(){
                fireCooldown += 1;

                if(controls.shoot && (fireCooldown >= fireReady)){
                    this.attacks.push(new Attack(this.x + 43, this.y + 13, 20, damage*0.5, 20, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 43, this.y + 71, 20, damage*0.5, 20, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 48, this.y + 19, 14, damage*2, 10, 5, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 48, this.y + 62, 14, damage*2, 10, 3, this.attacks, atkSprite));
                    fireCooldown = 0;
                }
            };
            
            break;
        case 2:
            this.width = 90;
            this.height = 73;
            
            spaceShipURI = "Resources/Class2.png";
            atkSpriteURI = "Resources/Laser2.png";
            this.hitSFX = hitSequence2;
            
            speed = 12;
            fireRate = 1.4;
            damage = 1.5;
            this.health = 8;
            
            this.shoot = function(){
                fireCooldown += 1;

                if(controls.shoot && (fireCooldown >= fireReady)){
                    this.attacks.push(new Attack(this.x + 54, this.y + 16, 15, damage, 20, 2, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 54, this.y + 18, 15, damage, 20, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 54, this.y + 50, 15, damage, 20, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 54, this.y + 52, 15, damage, 20, 3, this.attacks, atkSprite));
                    fireCooldown = 0;
                }
            };
            
            break;
        case 3:
            this.width = 91;
            this.height = 69;
            
            spaceShipURI = "Resources/Class3.png";
            atkSpriteURI = "Resources/Laser3.png";
            this.hitSFX = hitSequence3;
            
            speed = 15;
            fireRate = 4;
            damage = 1.5;
            this.health = 4;
            
            this.shoot = function(){
                fireCooldown += 1;

                if(controls.shoot && (fireCooldown >= fireReady)){
                    this.attacks.push(new Attack(this.x + 50, this.y + 16, 20, damage, 25, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 50, this.y + 48, 20, damage, 25, 3, this.attacks, atkSprite));
                    fireCooldown = 0;
                }
            };
            
            break;
        case 4:
            this.width = 92;
            this.height = 65;
            
            spaceShipURI = "Resources/Class4.png";
            atkSpriteURI = "Resources/Laser4.png";
            this.hitSFX = hitSequence4;
            
            speed = 17;
            fireRate = 1.5;
            damage = 4.5;
            this.health = 3;
            
            this.shoot = function(){
                fireCooldown += 1;

                if(controls.shoot && (fireCooldown >= fireReady)){
                    this.attacks.push(new Attack(this.x + 47, this.y + 7, 16, damage*0.5, 15, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + this.width, this.y + 32, 20, damage*1.5, 25, 3, this.attacks, atkSprite));
                    this.attacks.push(new Attack(this.x + 47, this.y + 55, 16, damage*0.5, 15, 3, this.attacks, atkSprite));
                    fireCooldown = 0;
                }
            };
            
            break;
    }
    
    fireReady = 60/fireRate;
    fireCooldown = fireReady;
    
    spaceShip.src = spaceShipURI;
    atkSprite.src = atkSpriteURI;
    
    this.render = function(){
        playerCanvasContext.drawImage(spaceShip, this.x, this.y, this.width, this.height);
        playerCanvasContext.fillStyle = "white";
        playerCanvasContext.font = "22px Arial";
        playerCanvasContext.fillText("P" + this.playerNumber, this.x + this.width - 10, this.y + this.height - 10);
        
        for(attack in this.attacks){
            this.attacks[attack].render();
        }
    };
    
    this.renderUI = function(position){
        UIseparation = Math.floor(UICanvas.width / (playerArray.length + 1));
        
        UIContext.fillStyle = "white";
        UIContext.font = "18px Arial";
        UIContext.fillText("Player " + this.playerNumber, UIseparation*position, 30);
        UIContext.fillText("Health: " + this.health, UIseparation*position, 50);
        UIContext.fillText("Score: " + this.score, UIseparation*position, 70);
    };
    
    this.move = function(){
        var movement = 0;
        
        if(controls.shift){
            movement = Math.floor(speed/2);
        }else{
            movement = speed;
        }
        
        if(controls.left){
            this.x -= movement;
            if(this.x < 0) this.x = 0;
        } else if(controls.right){
            this.x += movement;
            if(this.x + this.width> playerCanvas.width) this.x = playerCanvas.width - this.width;
        }
        
        if(controls.up){
            this.y -= movement;
            if(this.y < 0) this.y = 0;
        } else if(controls.down){
            this.y += movement;
            if(this.y + this.height > playerCanvas.height) this.y = playerCanvas.height - this.height;
        }
        
        for(attack in this.attacks){
            this.attacks[attack].move();
        }
    };
    
    this.damage = function(damage){
        this.health -= damage;
    };
    
    this.updateInput = function(playerInput){
        controls.left = playerInput.left;
        controls.right = playerInput.right;
        controls.up = playerInput.up;
        controls.down = playerInput.down;
        controls.shoot = playerInput.shoot;
        controls.shift = playerInput.shift;
    };
};