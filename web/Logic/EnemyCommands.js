/* global enemyArray, enemyCanvas */

function units2(){
    enemyArray.push(new Enemy(300));
    enemyArray.push(new Enemy(600));
};

function units4(){
    enemyArray.push(new Enemy(100));
    enemyArray.push(new Enemy(300));
    enemyArray.push(new Enemy(600));
    enemyArray.push(new Enemy(800));
    
    window.setTimeout(units2, 1000);
};

function units(y){
    for(var i = 0; i < y.length; i++){
        enemyArray.push(new Enemy(y[i]));
    }
};

function commandBranch(command){
    switch(command){
        case 0: 
            units2();
            break;
        case 1: 
            units4();
            break;
        case 2:
            units([100, 200, 300, 400, 500]);
            window.setTimeout(units, 1500, [400, 500, 600, 700, 800]);
            break;
    }
};