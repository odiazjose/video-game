var attackCanvas;

var enemyAttacks = [];

var sfxArray = [];

var expl = [];
    expl.push("Resources/Explosion/0.png");
    expl.push("Resources/Explosion/1.png");
    expl.push("Resources/Explosion/2.png");
    expl.push("Resources/Explosion/3.png");
    expl.push("Resources/Explosion/4.png");
    expl.push("Resources/Explosion/5.png");
    expl.push("Resources/Explosion/6.png");
    expl.push("Resources/Explosion/7.png");
    expl.push("Resources/Explosion/8.png");
    expl.push("Resources/Explosion/9.png");

var explSequence = [];

for(var i = 0; i < expl.length; i++){
    explSequence[i] = new Image();
    explSequence[i].src = expl[i];
}

var laserHit1 = [];
    laserHit1.push("Resources/LaserHit1/0.png");
    laserHit1.push("Resources/LaserHit1/1.png");
    laserHit1.push("Resources/LaserHit1/2.png");
    laserHit1.push("Resources/LaserHit1/3.png");
    laserHit1.push("Resources/LaserHit1/4.png");
    laserHit1.push("Resources/LaserHit1/5.png");
    laserHit1.push("Resources/LaserHit1/6.png");
    
var hitSequence1 = [];

for(var i = 0; i < laserHit1.length; i++){
    hitSequence1[i] = new Image();
    hitSequence1[i].src = laserHit1[i];
}

var laserHit2 = [];
    laserHit2.push("Resources/LaserHit2/0.png");
    laserHit2.push("Resources/LaserHit2/1.png");
    laserHit2.push("Resources/LaserHit2/2.png");
    laserHit2.push("Resources/LaserHit2/3.png");
    laserHit2.push("Resources/LaserHit2/4.png");
    laserHit2.push("Resources/LaserHit2/5.png");
    laserHit2.push("Resources/LaserHit2/6.png");
    
var hitSequence2 = [];

for(var i = 0; i < laserHit2.length; i++){
    hitSequence2[i] = new Image();
    hitSequence2[i].src = laserHit2[i];
}

var laserHit3 = [];
    laserHit3.push("Resources/LaserHit3/0.png");
    laserHit3.push("Resources/LaserHit3/1.png");
    laserHit3.push("Resources/LaserHit3/2.png");
    laserHit3.push("Resources/LaserHit3/3.png");
    laserHit3.push("Resources/LaserHit3/4.png");
    laserHit3.push("Resources/LaserHit3/5.png");
    laserHit3.push("Resources/LaserHit3/6.png");
    
var hitSequence3 = [];

for(var i = 0; i < laserHit3.length; i++){
    hitSequence3[i] = new Image();
    hitSequence3[i].src = laserHit3[i];
}

var laserHit4 = [];
    laserHit4.push("Resources/LaserHit4/0.png");
    laserHit4.push("Resources/LaserHit4/1.png");
    laserHit4.push("Resources/LaserHit4/2.png");
    laserHit4.push("Resources/LaserHit4/3.png");
    laserHit4.push("Resources/LaserHit4/4.png");
    laserHit4.push("Resources/LaserHit4/5.png");
    laserHit4.push("Resources/LaserHit4/6.png");
    
var hitSequence4 = [];

for(var i = 0; i < laserHit4.length; i++){
    hitSequence4[i] = new Image();
    hitSequence4[i].src = laserHit4[i];
}

var laserHit5 = [];
    laserHit5.push("Resources/LaserHit5/0.png");
    laserHit5.push("Resources/LaserHit5/1.png");
    laserHit5.push("Resources/LaserHit5/2.png");
    laserHit5.push("Resources/LaserHit5/3.png");
    laserHit5.push("Resources/LaserHit5/4.png");
    laserHit5.push("Resources/LaserHit5/5.png");
    laserHit5.push("Resources/LaserHit5/6.png");
    
var hitSequence5 = [];

for(var i = 0; i < laserHit5.length; i++){
    hitSequence5[i] = new Image();
    hitSequence5[i].src = laserHit5[i];
}