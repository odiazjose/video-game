/* global attackCanvas */

var Attack = function(posX, posY, speed, damage, width, height, attackArray, sprite){
    this.x = posX;
    this.y = posY;
    this.damage = damage;
    
    this.width = width;
    this.height = height;
    
    this.hit = false;
    
    var attackCanvasCtx = attackCanvas.getContext("2d");
    
    this.render = function(){
        attackCanvasCtx.drawImage(sprite, this.x, this.y, this.width, this.height);
    };
    
    this.move = function(){
        this.x += speed;
    };
};