var rgb = [];
    rgb.push("215, 215, 215");
    rgb.push("225, 225, 225");
    rgb.push("235, 235, 235");
    rgb.push("245, 245, 245");
    rgb.push("255, 255, 255");
    rgb.push("255, 255, 153");
    rgb.push("255, 255, 204");
    rgb.push("224, 255, 255");
    rgb.push("255, 204, 204");
    rgb.push("204, 255, 204");

var degrees = [];
    degrees['90'] = 0.5*Math.PI;
    degrees['180'] = Math.PI;
    degrees['270'] = 1.5*Math.PI;

var Star = function(starCanvas){
    var x = Math.random() * 1600;
    var y = Math.random() * starCanvas.height;
    var radius = (Math.random() * 4) + 5;
    
    var xLeft = x - radius;
    var xRight = x + radius;
    var yUp = y - radius;
    var yDown = y + radius;
    
    var speed = (Math.random() * 2) + 1;
    
    var color = "rgb(" + rgb[Math.round(Math.random() * rgb.length)] + ")";
    
    var canvasContext = starCanvas.getContext("2d");
    
    this.shuffleColors = function(){
        color = "rgb(" + rgb[Math.round(Math.random() * rgb.length)] + ")";
    };
    
    this.move = function(){
        x -= speed;
        
        xLeft = x - radius;
        xRight = x + radius;
        
        if(x < -radius){
            x = starCanvas.width;;
            y = Math.random() * starCanvas.height;
            radius = (Math.random() * 10) + 1;
            this.shuffleColors();
            
            yUp = y - radius;   
            yDown = y + radius;
        }
        
    };
    
    this.render = function(){
        canvasContext.beginPath();
        canvasContext.arc(xRight, yDown, radius, degrees['180'], degrees['270']);
        canvasContext.arc(xLeft, yDown, radius, degrees['270'], 0);
        canvasContext.arc(xLeft, yUp, radius, 0 , degrees['90']);
        canvasContext.arc(xRight, yUp, radius, degrees['90'], degrees['180']);
        canvasContext.fillStyle = color;
        canvasContext.fill();
        canvasContext.closePath();
    };
};