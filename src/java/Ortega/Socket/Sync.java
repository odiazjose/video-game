package Ortega.Socket;

import static Ortega.Socket.GameClientSession.gameClients;
import java.io.IOException;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/Sync")
public class Sync {
    @OnMessage
    public void onMessage(String message, Session session) throws IOException{
        synchronized(gameClients){
            for(Session s: gameClients){
                s.getBasicRemote().sendText(message);
            }
        }    
    }
}