package Ortega.Socket;

import Ortega.JSONBuilder.JSONBuilder;
import static Ortega.Socket.GameClientSession.gameClients;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/EnemyControl")
public class EnemyControl {
    private static JSONBuilder builder = new JSONBuilder();
    
    private static Integer command = 0;
    
    private static Random randomNumber = new Random();
    private static Integer commandAmount = 3;
    
    private static ScheduledExecutorService enemyTimer = Executors.newSingleThreadScheduledExecutor();
    private static Boolean timerOn = false; 
    
    private void randomizeCommand(){
        command = randomNumber.nextInt(commandAmount);
        
        builder.add("Action", "EnemyAction");
        builder.add("Command", command);
        
        synchronized(gameClients){
            for(Session s: gameClients){
                try{
                    s.getBasicRemote().sendText(builder.build());
                }catch(IOException e){
                    System.out.printf(e.getMessage());
                }
            }
        }
        
        builder.clear();
    }
    
    private void startTimer(){
        enemyTimer.scheduleAtFixedRate(
                () -> randomizeCommand(), 5, 5, TimeUnit.SECONDS);
    }
    
    private void stopTimer(){
        enemyTimer.shutdown();
    }

    @OnOpen
    public void onOpen(Session sudo) throws IOException {
        if(!timerOn){
            timerOn = true;
            
            startTimer();
        }
    }

    @OnClose
    public void onClose(Session gameClientSession) {
        Integer openSessions = 0;
        
        for(Session s: gameClients){
            openSessions++;
        }
        
        if(openSessions == 0){
            stopTimer();
            timerOn = false;
        }
    }
}