package Ortega.Socket;

import static Ortega.Socket.GameClientSession.gameClients;
import static Ortega.Socket.PlayerSession.players;
import java.io.IOException;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/JoinHandler")
public class JoinHandler {
    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        if(message.contains("GameClient")){
            synchronized(gameClients){
                for(Session s: gameClients){
                    s.getBasicRemote().sendText(message);
                }
            }
        }else if(message.contains("JoystickClient")){
            synchronized(players){
                for(Session s: players){
                    s.getBasicRemote().sendText(message);
                }
            }
        }
    }
}