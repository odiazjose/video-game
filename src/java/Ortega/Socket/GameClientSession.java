package Ortega.Socket;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/GameClientSession")
public class GameClientSession {
    public static Set<Session> gameClients = Collections.synchronizedSet(new HashSet<Session>());
    
    @OnOpen
    public void onOpen(Session gameClientSession) {
        gameClients.add(gameClientSession);
    }

    @OnClose
    public void onClose(Session gameClientSession) {
        gameClients.remove(gameClientSession);
    }
}