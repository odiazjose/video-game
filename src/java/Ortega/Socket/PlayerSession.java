package Ortega.Socket;

import Ortega.JSONBuilder.JSONBuilder;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/PlayerSession")
public class PlayerSession {
    public static Set<Session> players = Collections.synchronizedSet(new HashSet<Session>());
    
    @OnOpen
    public void onOpen(Session playerSession) throws IOException {
        players.add(playerSession);
        JSONBuilder builder = new JSONBuilder();
        builder.add("Action", "SetPlayerID");
        builder.add("setPlayerSessionID", playerSession.getId());
            
        playerSession.getBasicRemote().sendText(builder.build());
    }

    @OnClose
    public void onClose(Session playerSession) {
        players.remove(playerSession);
    }
}